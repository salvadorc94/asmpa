package com.example.asmpa


import android.content.*
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.example.asmpa.activity.ShowInfoActivity
import com.example.asmpa.databinding.ActivityMainBinding
import com.example.asmpa.model.Song
import com.example.asmpa.service.MusicService
import com.example.asmpa.service.NotificationService
import com.example.asmpa.util.*


class MainActivity : AppCompatActivity(), ServiceConnection {
    private var currentSong = 0
    private var toggleButton = true

    private lateinit var binding: ActivityMainBinding
    private var mService: MusicService? = null
    private lateinit var receiver: BroadcastReceiver
    private var mBound: Boolean = false

    private val updateHandler: Handler = Handler(Looper.getMainLooper())
    private lateinit var runnable: Runnable

    private val songs = ArrayList<Song>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initSong(R.raw.kalimba)
        initSong(R.raw.invincible)
        initSong(R.raw.stayinalive)

        // Bind to LocalService
        val intent = Intent(this, MusicService::class.java).also { intent ->
            bindService(intent, this, Context.BIND_AUTO_CREATE)
        }
        startService(intent)

        //Broadcast on Main
        val filter = IntentFilter()
        filter.addAction(NOTIFICATION_BUTTON)
        filter.addAction(SONG_FINISHED)
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent?.action.equals(SONG_FINISHED)) {
                    if (currentSong >= songs.size - 1) {
                        currentSong = songs.size - 1
                        mService?.pauseSound()
                        toggleButton = !toggleButton
                    } else {
                        currentSong += 1
                        mService?.switchSong(songs[currentSong])
                        mService?.play(songs[currentSong])
                    }
                    updateNotification(toggleButton)
                    updateView()
                }
                if (intent?.action.equals(NOTIFICATION_BUTTON)) {
                    toggleButton = if (toggleButton) {
                        mService?.play(song = songs[currentSong])
                        !toggleButton
                    } else {
                        mService?.pauseSound()
                        !toggleButton
                    }
                    updateNotification(toggleButton)
                    updateView()
                }
            }
        }
        registerReceiver(receiver, filter)

        currentSong = getPreference().first
        toggleButton = getPreference().second

        updateNotification(toggleButton)
        updateView()

        binding.playPauseButton.setOnClickListener {
            toggleButton = if (toggleButton) {
                mService?.play(song = songs[currentSong])
                !toggleButton
            } else {
                mService?.pauseSound()
                !toggleButton
            }
            initSeekBar()
            updateNotification(toggleButton)
            updateView()
        }

        binding.nextButton.setOnClickListener {
            if (currentSong >= songs.size - 1) {
                currentSong = DEFAULT_SONG_VALUE
            } else {
                currentSong += 1
            }
            updateNotification(toggleButton)
            updateView()
            mService?.switchSong(songs[currentSong])
            initSeekBar()
        }

        binding.backButton.setOnClickListener {
            if (currentSong <= DEFAULT_SONG_VALUE) {
                currentSong = songs.size - 1
            } else {
                currentSong -= 1
            }
            updateNotification(toggleButton)
            updateView()
            mService?.switchSong(songs[currentSong])
            initSeekBar()
        }

    }

    private fun initSeekBar() {
        runnable = Runnable {
            binding.seekBar.progress = mService?.player?.currentPosition ?: 0
            binding.seekBar.max = mService?.player?.duration ?: 0
            updateHandler.postDelayed(runnable, 0)
        }
        updateHandler.postDelayed(runnable, 0)

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) mService?.player?.seekTo(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    override fun onStop() {
        super.onStop()
        savePreference()
    }

    override fun onDestroy() {
        super.onDestroy()
        savePreference()
        unregisterReceiver(receiver)
    }

    private fun savePreference() {
        val playerState: SharedPreferences = getSharedPreferences(PLAYER_STATE, MODE_PRIVATE)
        playerState.edit().putInt(CURRENT_SONG, currentSong).apply()
        playerState.edit().putBoolean(BUTTON_STATUS, toggleButton).apply()
    }

    private fun getPreference(): Pair<Int, Boolean> {
        val playerState: SharedPreferences = getSharedPreferences(PLAYER_STATE, MODE_PRIVATE)
        val songNumber = playerState.getInt(CURRENT_SONG, DEFAULT_SONG_VALUE)
        val buttonStatus = playerState.getBoolean(BUTTON_STATUS, DEFAULT_BUTTON_VALUE)
        return Pair(songNumber, buttonStatus)
    }

    private fun updateView() {
        binding.imageView.setImageBitmap(songs[currentSong].image?.let {
            songs[currentSong].returnBitMap(
                it
            )
        })
        binding.textViewSong.text = songs[currentSong].title
        binding.textViewAlbum.text = songs[currentSong].album
        binding.textViewArtist.text = songs[currentSong].artist
        if (toggleButton) {
            binding.playPauseButton.setImageResource(R.drawable.baseline_play_circle_filled_24)
        } else {
            binding.playPauseButton.setImageResource(R.drawable.baseline_pause_circle_filled_24)
        }
    }

    fun showInfo(view: View) {
        val intent = Intent(this, ShowInfoActivity::class.java).apply {
            putExtra(EXTRA_SONG, songs[currentSong])
        }
        startActivity(intent)
    }

    private fun updateNotification(buttonStatus: Boolean) {
        val myServiceIntent = Intent(this, NotificationService::class.java)
        myServiceIntent.action = ACTIONS_START_FOREGROUND
        myServiceIntent.putExtra(BUTTON_STATUS, buttonStatus)
        myServiceIntent.putExtra(EXTRA_SONG, songs[currentSong])
        startService(myServiceIntent)
    }

    private fun initSong(id: Int) {
        val mmr = MediaMetadataRetriever()
        val uri = Uri.parse("$PACKAGE$id")
        mmr.setDataSource(application, uri)
        songs.add(
            Song(
                title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE),
                artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST),
                album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM),
                year = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR),
                genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE),
                image = mmr.embeddedPicture,
                song = id,
            )
        )

    }

    override fun onServiceConnected(className: ComponentName, service: IBinder) {
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        val binder = service as MusicService.LocalBinder
        mService = binder.getService()
        mBound = true
        initSeekBar()
    }

    override fun onServiceDisconnected(arg0: ComponentName) {
        mBound = false
    }


}