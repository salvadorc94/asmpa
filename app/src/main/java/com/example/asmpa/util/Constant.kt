package com.example.asmpa.util

const val EXTRA_SONG = "com.example.asmpa.SONG"
const val PLAYER_STATE = "com.example.asmpa.PLAYER_STATE"
const val CURRENT_SONG = "com.example.asmpa.CURRENT_SONG"
const val BUTTON_STATUS = "com.example.asmpa.BUTTON_STATUS"
const val DEFAULT_SONG_VALUE = 0
const val DEFAULT_BUTTON_VALUE = true
const val CHANNEL_ID = "com.example.asmpa.CHANNEL_ID"
const val FOREGROUND_ID = 1
const val ACTIONS_START_FOREGROUND = "com.example.asmmpa.START_FOREGROUND"
const val NOTIFICATION_BUTTON = "com.example.asmpa.NOTIFICATION"
const val PACKAGE = "android.resource://com.example.asmpa/"
const val SONG_FINISHED = "com.example.asmpa.SONG_FINISHED"