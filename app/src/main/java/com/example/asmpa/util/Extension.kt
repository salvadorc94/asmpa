package com.example.asmpa.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.asmpa.model.Song

fun Song.returnBitMap(byteArray: ByteArray): Bitmap {
    return BitmapFactory.decodeByteArray(
        byteArray,
        0,
        byteArray.size
    )
}
