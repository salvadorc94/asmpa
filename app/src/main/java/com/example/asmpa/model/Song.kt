package com.example.asmpa.model

import java.io.Serializable

class Song(
        val title: String?,
        val artist: String?,
        val album: String?,
        val year: String?,
        val genre: String?,
        val image: ByteArray?,
        val song: Int?,
) : Serializable