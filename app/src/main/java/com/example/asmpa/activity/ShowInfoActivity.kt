package com.example.asmpa.activity

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.asmpa.databinding.ActivityShowInfoBinding
import com.example.asmpa.model.Song
import com.example.asmpa.util.EXTRA_SONG
import com.example.asmpa.util.returnBitMap

class ShowInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityShowInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Get the Intent that started this activity and extract the string
        val message = intent.getSerializableExtra(EXTRA_SONG) as? Song?

        if (message != null) {
            binding.backgroundImage.setImageBitmap(message.image?.let { message.returnBitMap(it)})
            binding.textTitle.text = message.title
            binding.textAlbum.text = message.album
            binding.textArtist.text = message.artist
            binding.textGenre.text = message.genre
            binding.textYear.text = message.year.toString()
        }
    }
}