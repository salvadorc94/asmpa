package com.example.asmpa.service

import android.app.NotificationChannel
import android.app.NotificationManager
import com.example.asmpa.R
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.example.asmpa.MainActivity
import com.example.asmpa.model.Song
import com.example.asmpa.util.*


class NotificationService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onCreate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                R.string.notification_name.toString(),
                NotificationManager.IMPORTANCE_LOW
            )

            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(serviceChannel)
        }
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val song = intent?.getSerializableExtra(EXTRA_SONG) as? Song?
        val status = intent?.getBooleanExtra(BUTTON_STATUS, DEFAULT_BUTTON_VALUE)

        val text = if (status == true) "PLAY" else "PAUSE"

        val title = song?.title
        val artist = song?.artist
        val album = song?.album

        val testIntent = Intent().also {
            it.action = NOTIFICATION_BUTTON
        }
        val testPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(this, 0, testIntent, 0)
        if (intent?.action.equals(ACTIONS_START_FOREGROUND)) {

            val artwork = song?.image?.let { song.returnBitMap(it) }

            val notificationIntent = Intent(this, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(artist)
                .setSubText(album)
                .setLargeIcon(artwork)
                .addAction(R.drawable.baseline_play_circle_filled_24, text, testPendingIntent)
                .setSmallIcon(R.drawable.baseline_play_circle_filled_24)
                .setContentIntent(pendingIntent)
                .build()
            startForeground(FOREGROUND_ID, notification)
        }

        return START_NOT_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        stopForeground(true)
        stopSelf()
    }
}