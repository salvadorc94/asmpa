package com.example.asmpa.service

import android.app.*
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import com.example.asmpa.*
import com.example.asmpa.model.Song
import com.example.asmpa.util.*

class MusicService : Service() {

    private val binder = LocalBinder()

    var player: MediaPlayer? = null

    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): MusicService = this@MusicService
    }

    override fun onBind(intent: Intent?): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        getSharedPreferences(PLAYER_STATE, MODE_PRIVATE).edit()
            .putBoolean(BUTTON_STATUS, DEFAULT_BUTTON_VALUE).apply()
        if (player != null) {
            player?.stop()
            player?.release()
            player = null
        }
    }

    fun play(song: Song) {
        if (player == null) {
            player = song.song?.let { MediaPlayer.create(this, it) }
            setOnCompleteListener()
            player?.start()
        } else {
            player?.start()
        }

    }

    fun pauseSound() {
        if (player != null && player?.isPlaying == true) player?.pause()
    }


    fun switchSong(song: Song) {
        val wasSongActive = player?.isPlaying
        player?.stop()
        player?.reset()
        player?.release()
        player = song.song?.let { MediaPlayer.create(this, it) }
        setOnCompleteListener()
        if (wasSongActive == true) {
            player?.start()
        }
    }

    private fun setOnCompleteListener() {
        player?.setOnCompletionListener {
            Intent().also { intent ->
                intent.action = SONG_FINISHED
                sendBroadcast(intent)
            }
        }
    }

}